#pragma once
#include <openssl/conf.h>
#include <openssl/evp.h>
#include <openssl/err.h>
#include <openssl/rsa.h>
#include <openssl/pem.h>
#include <openssl/sha.h>
#include <openssl/aes.h>
#include <iostream>
#include <string>
#include <memory>
#include <cstdio>

typedef std::basic_string<unsigned char> ustring;

ustring ustr(std::string str);
void initOpenSSL();
void freeOpenSSL();

class pairRSA {
	RSA* rsa;
	public:
	pairRSA();
	pairRSA(size_t bits, size_t exponent);
	pairRSA(ustring& key, bool pub);
	RSA* get();
	ustring toUstring(bool pub);
	size_t size();
	ustring encrypt(const ustring data, int data_len, bool pub);
	ustring decrypt(const ustring enc_data, int data_len, bool pub);
	pairRSA& operator= (pairRSA&& other);
	~pairRSA();	
};

ustring encryptAES(const ustring plaintext, int plaintext_len, const ustring key);
ustring decryptAES(const ustring ciphertext, int ciphertext_len, const ustring key);
ustring sha256(const ustring line, size_t len);
