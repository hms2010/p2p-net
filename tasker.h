#pragma once
#include <atomic>
#include <vector>
#include <string>
#include <memory>
#include <cstdlib>
#include <boost/thread.hpp> 

typedef void (*taskHandle)(std::vector<std::string>&);

class task {
	size_t _task_id;
	std::vector<std::string> _data;
	taskHandle _handle;
	public:
	task();
	task(const task& other);
	task(task&& other);
	task(taskHandle function);
	void push_back(std::string data);
	void operator ()();
	task& operator =(task&& other);
};


class tasker {
	std::atomic<bool> _status;
	std::atomic<bool> _working;
	boost::mutex _mut;
	std::vector<task> _tasks;
	std::unique_ptr<boost::thread> _thread;
	void run();
	public:
	tasker();
	void push_back(task&& data);
	void start();
	void stop();
	bool status();
	bool working();
};
