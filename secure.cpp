#include "secure.h"
#if defined(__WIN32__)
#include "applink.c"
#endif

using CTX_ptr = std::unique_ptr<EVP_CIPHER_CTX, decltype(&::EVP_CIPHER_CTX_free)>;
const unsigned char* iv = (const unsigned char*)"ThisIsOurSpace :3";


ustring ustr(std::string str) {
	return ustring((const unsigned char*)str.c_str(), str.length());
}


void initOpenSSL() {
	ERR_load_crypto_strings();
	OpenSSL_add_all_algorithms();
}

void freeOpenSSL() {
	EVP_cleanup();
	ERR_free_strings();
}

pairRSA::pairRSA() : rsa(RSA_new()) {}

pairRSA::pairRSA(size_t bits, size_t exponent) : rsa(RSA_new()) {
	BIGNUM* e = BN_new();
	if(BN_set_word(e,exponent))
		RSA_generate_key_ex(rsa, bits, e, NULL);
	BN_free(e);
}

pairRSA::pairRSA(ustring& key, bool pub) : rsa(NULL) {
	BIO *keybio;
	keybio = BIO_new_mem_buf((const char*)key.c_str(), -1);
	if(pub)
		rsa = PEM_read_bio_RSAPublicKey(keybio, &rsa, NULL, NULL);
	else
		rsa = PEM_read_bio_RSAPrivateKey(keybio, &rsa, NULL, NULL);
	BIO_free_all(keybio);
}

RSA* pairRSA::get() {
	return rsa;
}

ustring pairRSA::toUstring(bool pub) {
	BIO* bp = BIO_new(BIO_s_mem());
	if(pub) {
		if(!PEM_write_bio_RSAPublicKey(bp, rsa)) {
			BIO_free_all(bp);
			return ustring();
		}
	}
	else
		if(!PEM_write_bio_RSAPrivateKey(bp, rsa, NULL, NULL, 0, NULL, NULL)) {
			BIO_free_all(bp);
			return ustring();
		}
	size_t len = BIO_pending(bp);
	unsigned char *buf = new unsigned char[len];
	BIO_read(bp, buf, len);
	BIO_free_all(bp);
	return ustring(std::move(buf), len); 
} 

size_t pairRSA::size() {
	return RSA_size(rsa);
}

ustring pairRSA::encrypt(const ustring data, int data_len, bool pub) {
	unsigned char* buf = new unsigned char[RSA_size(rsa)];
	if(pub) {
		if(RSA_public_encrypt(data_len, data.c_str(), buf, rsa, RSA_PKCS1_PADDING)==-1) {
			delete [] buf;
			return ustring();
		}
	}
	else
		if(RSA_private_encrypt(data_len, data.c_str(), buf, rsa, RSA_PKCS1_PADDING)==-1) {
			delete [] buf;
			return ustring();
		}
	return ustring(std::move(buf), RSA_size(rsa));
}

ustring pairRSA::decrypt(const ustring enc_data, int data_len, bool pub) {
    unsigned char* buf = new unsigned char[RSA_size(rsa)];
	size_t len;
	if(pub) {
		len = RSA_public_decrypt(data_len, enc_data.c_str(), buf, rsa, RSA_PKCS1_PADDING);
		if(len==-1){
			delete [] buf;
			return ustring();
		}
	}
	else {
		len = RSA_private_decrypt(data_len, enc_data.c_str(), buf, rsa, RSA_PKCS1_PADDING);
		if(len==-1) {
			delete [] buf;
			return ustring();
		}
	}
	return ustring(std::move(buf), len);
}
pairRSA& pairRSA::operator= (pairRSA&& other) {
	RSA_free(rsa);
	rsa = other.rsa;
	other.rsa = NULL;
	return *this;
}

pairRSA::~pairRSA() {
	RSA_free(rsa);
}

ustring encryptAES(const ustring plaintext, int plaintext_len, const ustring key) {
	CTX_ptr ctx(EVP_CIPHER_CTX_new(), ::EVP_CIPHER_CTX_free);
	unsigned char* buf = new unsigned char[plaintext_len+AES_BLOCK_SIZE];
	int len;
	int total_len;
	
	if(EVP_EncryptInit_ex(ctx.get(), EVP_aes_256_cbc(), NULL, key.c_str(), iv) != 1) {
		delete [] buf;
		return ustring();
	}

	if(EVP_EncryptUpdate(ctx.get(), buf, &len, plaintext.c_str(), plaintext_len) != 1){
		delete [] buf;
		return ustring();
	}
	total_len = len;
	if(EVP_EncryptFinal_ex(ctx.get(), buf + len, &len) != 1) {
		delete [] buf;
		return ustring();
	}
	total_len += len;
	return ustring(std::move(buf), total_len);
}


ustring decryptAES(const ustring ciphertext, int ciphertext_len, const ustring key) {
	CTX_ptr ctx(EVP_CIPHER_CTX_new(), ::EVP_CIPHER_CTX_free);
	unsigned char* buf = new unsigned char[ciphertext_len+AES_BLOCK_SIZE];
	int len;
	int total_len;
	
	if(EVP_DecryptInit_ex(ctx.get(), EVP_aes_256_cbc(), NULL, key.c_str(), iv) != 1){
		delete [] buf;
		return ustring();
	}

	if(EVP_DecryptUpdate(ctx.get(), buf, &len, ciphertext.c_str(), ciphertext_len) != 1){
		delete [] buf;
		return ustring();
	}
	total_len = len;
	
	if(EVP_DecryptFinal_ex(ctx.get(), buf + len, &len) != 1) {
		delete [] buf;
		return ustring();
	}
	total_len += len;
	return ustring(std::move(buf), total_len);
}


ustring sha256(const ustring line, size_t len) {
    unsigned char hash[SHA256_DIGEST_LENGTH];
    SHA256_CTX s256;
    SHA256_Init(&s256);
    SHA256_Update(&s256, line.c_str(), len);
    SHA256_Final(hash, &s256);
	return ustring(hash, SHA256_DIGEST_LENGTH);
}
