#include "p2p_proxy.h"



void closeProxyConnection(vector<std::shared_ptr<clientData>>& clients, shared_ptr<boost::mutex> mut, std::shared_ptr<clientData> current) {
	boost::unique_lock<boost::mutex> lk(*mut);
	current->sock->close();
	for(size_t i = 0; i < clients.size(); ++i) 
		if(clients[i] == current) {
			clients.erase(clients.begin() + i);
			break;
		}
}


void handleByProxy(vector<std::shared_ptr<clientData>>& clients, shared_ptr<boost::mutex> mut, std::shared_ptr<clientData> current) {
	
	/*mesmut.lock();
	size_t unique_ind = lastind++;
	if(lastind==100)
		lastind=0;
	cout << unique_ind << "\t" << "[CONNECTED]" << endl;
	mesmut.unlock();*/
	
	boost::system::error_code ec;
	io_service _ios;
	ip::tcp::socket sock(_ios);
	bool cont = true;
	sock.close();
	while(cont) {
		netBuffer inbuf;
		netBuffer outbuf;
		string host;
		string addr;
		string port = "80";
		bool fm = true;
		do {
			size_t n = read_until(*(current->sock), inbuf.get(), "\n", ec);
			if(ec) {
				//mesmut.lock();
				//cout << unique_ind << "\t" << "Can't read request: " << ec.message() << endl;
				//mesmut.unlock();
				sock.close();
				closeProxyConnection(clients, mut, current);
				return;
			}
			string line(n, ' ');
			inbuf.read(&line[0], n);
			
			stringstream ss(line);
			string type;
			ss >> type;
			if(type=="Host:") {
				ss >> host;
				size_t i = host.find(":");
				if(i!=string::npos) {
					port = host.substr(i+1);
					host = host.substr(0, i);
				}
			}
			if(fm) {
				ss >> addr;
				if(addr.substr(0, 7)=="http://")
					addr = addr.substr(7);
				size_t i = addr.find("/");
				if(i!=string::npos) 
					addr = addr.substr(i);
				line = type + " " + addr+" ";
				ss >> type;
				line += type+"\r\n";
			}
			else if(type=="Proxy-Connection:") 
				line = line.substr(6);
			if(line=="Connection: keep-alive\r\n") 
				cont = true;
			else if(line=="Connection: close\r\n") 
				cont = false;
			
			outbuf.write(line.c_str(), line.length());
			/*mesmut.lock();
			if(fm) {
				fm = false;
				cout << unique_ind << "\t" << "[REQUEST]" << endl;
			}
			cout << unique_ind << "\t" << line;
			mesmut.unlock();*/
			if(fm)
				fm = false;	
			
			if(line=="\r\n")
				break;
		}while(1);
		
		if(extension(path(host))==".p2p") {
			host = "127.0.0.1";
			port = "2323";
			////outbuf.clear();
			//outbuf.write("HTTP/1.1 200 ok\r\nContent-Length: 11\r\nConnection: close\r\n\r\nHello world", 69);
			//write(*(current->sock), outbuf.get(), ec);
			//closeProxyConnection(clients, mut, current);
			//return;
		}
		
		if(!sock.is_open()) {
			ip::tcp::resolver resolver(_ios);
			ip::tcp::resolver::query query(host, port);
			ip::tcp::resolver::iterator itr = resolver.resolve(query, ec);
			if(ec) {
				//mesmut.lock();
				//cout << unique_ind << "\t" << "Can't resolve query: " << ec.message() << endl;
				//mesmut.unlock();
				closeProxyConnection(clients, mut, current);
				return;
			}
			ip::tcp::endpoint ep = *itr;
			sock.connect(ep, ec);
			if(ec) {
				//mesmut.lock();
				//cout << unique_ind << "\t" << "Can't connect to host: " << ec.message() << endl;
				//mesmut.unlock();
				sock.close();
				closeProxyConnection(clients, mut, current);
				return;
			}
		}
		write(sock, outbuf.get(), ec);
		if(ec) {
			//mesmut.lock();
			//cout << unique_ind << "\t" << "Can't write request: "  << ec.message() << endl;
			//mesmut.unlock();
			sock.close();
			closeProxyConnection(clients, mut, current);
			return;
		}
		inbuf.clear();
		bool chunked = false;
		size_t length = 0;
		
		fm = true;
		do {
			size_t n = read_until(sock, inbuf.get(), "\r\n", ec);
			if(ec) {
				//mesmut.lock();
				//cout << unique_ind << "\t" << "Can't read answer: " << ec.message() << endl;
				//mesmut.unlock();
				sock.close();
				closeProxyConnection(clients, mut, current);
				return;
			}
			string line(n, ' ');
			inbuf.read(&line[0], n);
			
			if(line=="Transfer-Encoding: chunked\r\n") 
				chunked = true;
			else if(line=="Connection: keep-alive\r\n") 
				cont = true;
			else if(line=="Connection: close\r\n") 
				cont = false;
			
			stringstream ss(line);
			string type;
			ss >> type;
			if(type=="Content-Length:") {
				ss >> type;
				length = stoi(type);
			}
			if(fm) 
				fm = false;
			/*mesmut.lock();
			if(fm) {
				fm = false;
				cout << unique_ind << "\t" << "[ANSWER]" << endl;
			}
			cout << unique_ind << "\t" << line;
			mesmut.unlock();*/
			outbuf.write(line.c_str(), n);
			write(*(current->sock), outbuf.get(), ec);
			if(ec) {
				//mesmut.lock();
				//cout << unique_ind << "\t" << "Can't write answer: "  << ec.message() << endl;
				//mesmut.unlock();
				sock.close();
				closeProxyConnection(clients, mut, current);
				return;
			}
			if(line=="\r\n")
				break;
		}while(1);
		
		if(chunked) {
			do {
				size_t n = read_until(sock, inbuf.get(), "\n", ec);
				if(ec) {
					//mesmut.lock();
					//cout << unique_ind << "\t" << "Can't read answer chunk size: " << ec.message() << endl;
					//mesmut.unlock();
					sock.close();
					closeProxyConnection(clients, mut, current);
					return;
				}
				string line(n, ' ');
				inbuf.read(&line[0], n);
				outbuf.write(line.c_str(), n);
				sscanf(line.c_str(), "%x", &length);
				read(sock, inbuf.get(), transfer_exactly(length+2-inbuf.size()), ec);
				if(ec) {
					//mesmut.lock();
					//cout << unique_ind << "\t" << "Can't read answer chunk: " << ec.message() << endl;
					//mesmut.unlock();
					sock.close();
					closeProxyConnection(clients, mut, current);
					return;
				}
				
				line = string(length+2, ' ');
				inbuf.read(&line[0], length+2);
				outbuf.write(line.c_str(), length+2);
				write(*(current->sock), outbuf.get(), ec);
				if(ec) {
					//mesmut.lock();
					//cout << unique_ind << "\t" << "Can't write answer chunk: "  << ec.message() << endl;
					//mesmut.unlock();
					sock.close();
					closeProxyConnection(clients, mut, current);
					return;
				}
				if(line=="\r\n")
					break;
			}while(1);
		}
		else if(length){
			read(sock, inbuf.get(), transfer_exactly(length-inbuf.size()), ec);
			if(ec) {
				//mesmut.lock();
				//cout << unique_ind << "\t" << "Can't read answer data: " << ec.message() << endl;
				//mesmut.unlock();
				sock.close();
				closeProxyConnection(clients, mut, current);
				return;
			}
			string line(length, ' ');
			inbuf.read(&line[0], length);
			outbuf.write(line.c_str(), length);
			write(*(current->sock), outbuf.get(), ec);
			if(ec) {
				//mesmut.lock();
				//cout << unique_ind << "\t" << "Can't write answer data: "  << ec.message() << endl;
				//mesmut.unlock();
				sock.close();
				closeProxyConnection(clients, mut, current);
				return;
			}
		}
		
		//write(*(current->sock), outbuf.get(), ec);
		
	}
	sock.close();
	//mesmut.lock();
	//cout << unique_ind << "\t" << "[DISCONNECTED]" << endl;
	//mesmut.unlock();
	closeProxyConnection(clients, mut, current);
}
