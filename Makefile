CXX = g++
CXXFLAGS = -std=c++11 -g
LDFLAGS = -lboost_system -lboost_thread -lboost_chrono -lboost_filesystem -lboost_random -lboost_date_time -lpthread
ifeq ($(OS), Windows_NT)
CXXFLAGS += -I/include -U__STRICT_ANSI__ -D_WIN32_WINNT=0x0501
LDFLAGS += -L/lib -lws2_32 -lwsock32 -lpdcurses /lib/openssl.lib /lib/libcrypto.lib
else
LDFLAGS += -lncurses -lssl -lcrypto
endif

all: main.o clientData.o asyncServer.o asyncClient.o secure.o netBuffer.o cursesUtils.o tasker.o threadPool.o p2p_web.o p2p_proxy.o
	$(CXX) $^ -o p2p $(LDFLAGS)


clean:
	rm -rf *.o
