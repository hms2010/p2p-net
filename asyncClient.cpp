#include "asyncClient.h"



void asyncClient::run() {
	_threadPool.start(4); //TODO: Magic number
	_ios.run();
	_ios.reset();
	_status = false;
}

//asyncClient::asyncClient(void (*accept)(const boost::system::error_code&), void (*clientHandle)(std::vector<std::shared_ptr<clientData>>&, std::shared_ptr<boost::mutex>, std::shared_ptr<clientData>)): 
asyncClient::asyncClient(acceptFunction accept, handleFunction clientHandle): 
		_status(false), 
		_mutex(std::make_shared<boost::mutex>()),
		_accept(accept),
		_clientHandle(clientHandle){
}

void asyncClient::connect(std::string address, unsigned short port) {
	_mutex->lock();
	_clients.push_back(std::make_shared<clientData>(_ios, address, port));
	std::shared_ptr<clientData> temp = _clients[_clients.size()-1];
	_mutex->unlock();
	temp->sock->async_connect(*(temp->ep), [this, temp](const boost::system::error_code& ec) {
		_accept(ec);
		if(!ec)
			_threadPool.addWork(_clientHandle, _clients, _mutex, temp);
		else {
			boost::unique_lock<boost::mutex> lk(*_mutex);
			for(auto i = _clients.begin(); i != _clients.end(); ++i)
				if(*i == temp) {
					_clients.erase(i);
					break;
				}
			//for(size_t i=0; i<_clients.size(); ++i)
				//if(_clients[i]==temp) {
				//	_clients.erase(_clients.begin()+i);
				//	break;
				//}
		}
	});
}

void asyncClient::start() {
	_thread.reset(new boost::thread(&asyncClient::run, this));
	_status = true;
}

void asyncClient::stop() {
	if(_status) {
		_ios.stop();
		_thread->join();
		_threadPool.join();
		_threadPool.stop();
	}
}

bool asyncClient::status() {
	return _status;
}
