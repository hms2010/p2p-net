#include <iostream>
#include <string>
#include <vector>
#include <boost/asio.hpp>
#include <boost/filesystem.hpp>
#include "asyncServer.h"
#include "netBuffer.h"
using namespace std;
using namespace boost::asio;
using namespace boost::filesystem;

void handleByProxy(vector<std::shared_ptr<clientData>>& clients, shared_ptr<boost::mutex> mut, std::shared_ptr<clientData> current);
