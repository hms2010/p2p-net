#pragma once
#include "secure.h"
#include <istream>
#include <string>
#include <boost/asio.hpp>

class netBuffer {
	boost::asio::streambuf buf;
	std::iostream stream;
public:
	netBuffer();
	void writeByte(const uint8_t a); 
	void writeShort(const uint16_t a);
	void writeLong(const uint32_t a);
	void writeString(const std::string& str);
	void writeUstring(const ustring& str);
	uint8_t readByte();
	uint16_t readShort();
	uint32_t readLong();
	std::string readString();
	ustring readUstring();
	void read(void* data, size_t len);
	void write(const void* data, size_t len);
	void clear();
	size_t size();
	boost::asio::streambuf& get();
};
