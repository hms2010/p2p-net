#pragma once
#include "clientData.h"
#include <boost/asio.hpp>
#include <boost/thread.hpp>
#include <boost/bind.hpp>


class threadPool {
	boost::asio::io_service _ios;
	boost::thread_group _threadpool;
	std::unique_ptr<boost::asio::io_service::work> _dummy;
public:
	threadPool();
	void start(size_t n);
	void join();
	void stop();
	template <typename T, typename ... Args>
	void addWork(T func, Args ... args);
};

template <typename T, typename ... Args>
void threadPool::addWork(T func, Args ... args) {
	_ios.post(boost::bind(func, args...));
}
