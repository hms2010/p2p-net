#pragma once
#include <boost/thread/mutex.hpp>
#include <vector>
#include <string>
#include <atomic>

#if defined(__WIN32__)
#include <curses.h>
#else
#include <ncurses.h>
#endif

typedef std::basic_string<chtype> chstring;

chstring operator |(chstring a, int attr);
chstring chstr(const std::string& str, chtype attr=getattrs(stdscr));


struct window {
	WINDOW* decor;
	WINDOW* win;
	int cb;
	int fc;
	window(size_t h, size_t w, size_t y, size_t x, int CB, int FC);
	void update(size_t h, size_t w, size_t y, size_t x);
	void redraw();
	~window();
};

std::string dialogWindow(chstring name, size_t w, bool hidden, int CB, int FC);
void messageWindow(chstring name, chstring message, int CB, int FC);

struct menuElement {
	chstring data;
	bool select;
	int allign;
	menuElement(chstring Data, bool Select, int Allign);
};

class menuList {
	std::vector<menuElement> list;
	int n;
	public:
	menuList();
	void add(chstring line, bool sel, int allign);
	void add(std::string line, bool sel, int allign, chtype attr=getattrs(stdscr));
	void draw(WINDOW* win, size_t y, size_t x, size_t w=0);
	void clear();
	void operator =(int x);
	void operator ++();
	void operator --();
	operator int();
	chstring& operator [](size_t i);
};

class logger {
	std::atomic<bool> _status;
	std::vector<chstring> _log;
	boost::mutex _mut;
	WINDOW* _win;
	int _page;
	public:
	logger();
	void setWin(WINDOW* W);
	void start();
	void stop();
	void update(chstring str);
	void update();
	bool status();
	void operator ++();
	void operator --();
};