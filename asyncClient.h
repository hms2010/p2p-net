#pragma once
#include "clientData.h"
#include "threadPool.h"
#include <boost/thread.hpp>
#include <boost/asio.hpp>
#include <boost/system/error_code.hpp>
#include <atomic>
#include <memory>
#include <string>
using namespace boost::asio;

class asyncClient {
	std::atomic<bool> _status;
    std::unique_ptr<boost::thread> _thread;
    io_service _ios;
	std::vector<std::shared_ptr<clientData>> _clients;
	std::shared_ptr<boost::mutex> _mutex;
	threadPool _threadPool;
	acceptFunction _accept;
	handleFunction _clientHandle;
	void run();
	public:
	asyncClient(acceptFunction accept, handleFunction clientHandle);
	void connect(std::string arrdress, unsigned short port);
    void start();
    void stop();
	bool status();
};
