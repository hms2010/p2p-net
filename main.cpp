#include <iostream>
#include <vector>
#include <fstream>
#include <string>

#if defined(__WIN32__)
#include <windows.h>
#include <curses.h>
#else
#include <ncurses.h>
#endif

#include <boost/thread.hpp>
#include <boost/asio.hpp>
#include <boost/thread/mutex.hpp>
#include <boost/chrono.hpp>
#include <boost/date_time.hpp>
#include <boost/filesystem.hpp>
#include <boost/random/random_device.hpp>
#include <boost/random/uniform_int_distribution.hpp>

#include "asyncServer.h"
#include "asyncClient.h"
#include "secure.h"
#include "netBuffer.h"
#include "cursesUtils.h"
#include "tasker.h"
#include "p2p_proxy.h"
#include "p2p_web.h"


using namespace boost::filesystem;
using namespace boost::gregorian;
using namespace boost::posix_time;
using namespace boost::asio;
using namespace std;


ustring getRandUString(size_t n) {
	ustring line;
	ustring chars((const unsigned char*)"abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890");
    boost::random::random_device rng;
    boost::random::uniform_int_distribution<> index_dist(0, chars.size() - 1);
    for(int i = 0; i < n; ++i) {
        line += chars[index_dist(rng)];
    }
    return line;
}

ustring getRandUString(size_t mi, size_t ma) {
	boost::random::random_device rng;
    boost::random::uniform_int_distribution<> index_dist(mi, ma);
    return getRandUString(index_dist(rng));
}

logger _logger;
tasker _tasker;

void acceptByServer(const boost::system::error_code& ec) {
	if(ec) {
		_logger.update(chstr(ec.message(), COLOR_PAIR(3)));
		return;
	}
}

boost::mutex rsamut; //TODO: KOSTILI!!!
pairRSA localrsa; //TODO: change it
ustring ununame; //TODO: change it

void closeConnection(vector<std::shared_ptr<clientData>>& clients, shared_ptr<boost::mutex> mut, std::shared_ptr<clientData> current) {
	current->mut->lock();
	_logger.update(chstr("Disconnecting with ", COLOR_PAIR(2))+chstr(current->ep->address().to_string()));
	current->mut->unlock();
	boost::unique_lock<boost::mutex> lk(*mut);
	current->sock->close();
	for(size_t i = 0; i < clients.size(); ++i) 
		if(clients[i] == current) {
			clients.erase(clients.begin() + i);
			break;
		}
}


void handleByServer(vector<std::shared_ptr<clientData>>& clients, shared_ptr<boost::mutex> mut, std::shared_ptr<clientData> current) {
	current->mut->lock();
	_logger.update(chstr("New user connected: ", COLOR_PAIR(4)) + chstr(current->ep->address().to_string()));
	current->mut->unlock();
	
	
	netBuffer inbuf;
	netBuffer outbuf;
	boost::system::error_code ec;
	
	current->read(inbuf.get(), 1, ec);
	if(ec) {
		_logger.update(chstr(ec.message(), COLOR_PAIR(3)));
		closeConnection(clients, mut, current);
		return;
	}
	ustring sessionKey;
	ustring secureKey; //Unsecured

	switch(inbuf.readByte()) {
		
		//REGISTRATION:
		case 0: { 			
			_logger.update(chstr("Try to register...", COLOR_PAIR(4)));
			current->read(inbuf.get(), 2, ec);
			if(ec) {
				_logger.update(chstr(ec.message(), COLOR_PAIR(3)));
				closeConnection(clients, mut, current);
				return;
			}
			uint16_t n = inbuf.readShort();
			
			current->read(inbuf.get(), n, ec);
			if(ec) {
				_logger.update(chstr(ec.message(), COLOR_PAIR(3)));
				closeConnection(clients, mut, current);
				return;
			}
			ustring pub(n, ' ');
			inbuf.read(&pub[0], n);
					
			pairRSA rsa(pub, true);
			if(rsa.get() == NULL) {
				_logger.update(chstr("Public RSA key isn't valid", COLOR_PAIR(3)));
				outbuf.clear();
				outbuf.writeByte(0);
				current->write(outbuf.get(), ec);
				closeConnection(clients, mut, current);
				return;
			}
			
			ustring uname;
			if(secureKey != ustring()) { 
				outbuf.clear();
				outbuf.writeByte(2);
				current->write(outbuf.get(), ec);
				if(ec) {
					_logger.update(chstr(ec.message(), COLOR_PAIR(3)));
					closeConnection(clients, mut, current);
					return;
				}
				
				
				current->read(inbuf.get(), 2, ec);
				if(ec) {
					_logger.update(chstr(ec.message(), COLOR_PAIR(3)));
					closeConnection(clients, mut, current);
					return;
				}
				n = inbuf.readShort();
				
				current->read(inbuf.get(), n, ec);
				if(ec) {
					_logger.update(chstr(ec.message(), COLOR_PAIR(3)));
					closeConnection(clients, mut, current);
					return;
				}
				
				ustring data(n, ' ');
				inbuf.read(&data[0], n);
				uname = decryptAES(data, data.length(), secureKey);
				if(uname == ustring()) {
					_logger.update(chstr("AES decode error", COLOR_PAIR(3)));
					outbuf.clear();
					outbuf.writeByte(0);
					current->write(outbuf.get(), ec);
					closeConnection(clients, mut, current);
					return;
				}
			}
			else {
				ustring key = getRandUString(32);
				outbuf.clear();
				outbuf.writeByte(1);
				outbuf.write(rsa.encrypt(key, key.length(), true).c_str(), rsa.size());
				current->write(outbuf.get(), ec);
				if(ec) {
					_logger.update(chstr(ec.message(), COLOR_PAIR(3)));
					closeConnection(clients, mut, current);
					return;
				}
				
				current->read(inbuf.get(), 2, ec);
				if(ec) {
					_logger.update(chstr(ec.message(), COLOR_PAIR(3)));
					closeConnection(clients, mut, current);
					return;
				}
				n = inbuf.readShort();
				
				current->read(inbuf.get(), n, ec);
				if(ec) {
					_logger.update(chstr(ec.message(), COLOR_PAIR(3)));
					closeConnection(clients, mut, current);
					return;
				}
				ustring data(n, ' ');
				inbuf.read(&data[0], n);
				uname = decryptAES(data, data.length(), key);
				if(uname == ustring()) {
					_logger.update(chstr("AES decode error", COLOR_PAIR(3)));
					outbuf.clear();
					outbuf.writeByte(0);
					current->write(outbuf.get(), ec);
					closeConnection(clients, mut, current);
					return;
				}
			}
			
			path pth(path("users")/path(string((const char*)uname.c_str())+".pem"));
			if(exists(pth)) {
				_logger.update(chstr("User already exists", COLOR_PAIR(3)));
				outbuf.clear();
				outbuf.writeByte(0);
				current->write(outbuf.get(), ec);
				closeConnection(clients, mut, current);
				return;
			}
			
			std::ofstream file(pth.string().c_str(), ios_base::binary|ios_base::out);
			if(!file.is_open()) {
				_logger.update(chstr("Can't open file ", COLOR_PAIR(3)) + chstr(pth.string()));
				closeConnection(clients, mut, current); //Is it right?
				return;
			}
			file.write((char*)pub.c_str(), pub.length());
			file.close();
			
			pth = path("userdata")/path(string((const char*)uname.c_str()));
			create_directory(pth);
			pth /= path("perms.data");
			file.open(pth.string().c_str(), ios_base::binary|ios_base::out);
			if(!file.is_open()) {
				_logger.update(chstr("Can't open file ", COLOR_PAIR(3)) + chstr(pth.string()));
				closeConnection(clients, mut, current); //Is it right?
				return;
			}
			//TODO: Make rule config of net
			//[USERNAME][YYYYMMDDTHHMMSS][PERMISSIONS](IS TEMP){DATE OF EXPIRATION}[RSA SIGN]
			ustring data = uname + ustring((const unsigned char*)"\0", 1) + ustr(to_iso_string(second_clock::local_time())) + ustring((const unsigned char*)"\0\0", 2); // TODO: itn't nice...
			rsamut.lock();
			ustring sign = localrsa.encrypt(data, data.length(), false);
			rsamut.unlock();
			if(sign == ustring()) {
				file.close();
				_logger.update(chstr("Can't sign data", COLOR_PAIR(3)));
				closeConnection(clients, mut, current); //Is it right?
				return;
			}
			data += sign;
			file.write((char*)data.c_str(), data.length());
			file.close();
			_logger.update(chstr("User successfully registered: ", COLOR_PAIR(4))+chstr((const char*)uname.c_str()));
			
			sessionKey = getRandUString(32);
			outbuf.clear();
			outbuf.writeByte(1);
			outbuf.write(rsa.encrypt(sessionKey, sessionKey.length(), true).c_str(), rsa.size());
			current->write(outbuf.get(), ec);
			
			break;
		}
		
		//LOGGIN IN
		case 1: {
			_logger.update(chstr("Try to login...", COLOR_PAIR(4)));
			current->read(inbuf.get(), 32, ec); //HSA256
			ustring hash(32, ' ');
			inbuf.read(&hash[0], 32);
			if(ec) {
				_logger.update(chstr(ec.message(), COLOR_PAIR(3)));

				closeConnection(clients, mut, current);
				return;
			}
			path fname;
			ustring uname;
			for(auto a = directory_iterator(path("users")); a != directory_iterator(); ++a) {
				uname = ustr(a->path().stem().string());
				if(sha256(uname, uname.length())==hash) {
					fname = a->path();
					break;
				}
			}
			if(fname == path()) {
				_logger.update(chstr("User not exists", COLOR_PAIR(3)));
				outbuf.clear();
				outbuf.writeByte(0);
				current->write(outbuf.get(), ec);
				closeConnection(clients, mut, current);
				return;
			}
			std::ifstream file(fname.string().c_str(), ios_base::binary|ios_base::in);
			if(!file.is_open()) {
				_logger.update(chstr("Can't open file ", COLOR_PAIR(3)) + chstr(fname.string()));
				closeConnection(clients, mut, current); //Is it right?
				return;
			}
			file.seekg(0, ios_base::end);
			size_t n = file.tellg();
			file.seekg(0, ios_base::beg);
			ustring pub(n, ' ');
			file.read((char*)&pub[0], n);
			file.close();
			pairRSA rsa(pub, true);
			
			current->read(inbuf.get(), 15, ec); //YYYYMMDDTHHMMSS: 15 symbols
			if(ec) {
				_logger.update(chstr(ec.message(), COLOR_PAIR(3)));
				closeConnection(clients, mut, current);
				return;
			}
			string rectimestr(15, ' ');
			inbuf.read(&rectimestr[0], 15);
			ptime rectime = from_iso_string(rectimestr);
			if(second_clock::local_time() - rectime > seconds(10)) {
				_logger.update(chstr("Connection too slow", COLOR_PAIR(3)));
				outbuf.clear();
				outbuf.writeByte(2);
				current->write(outbuf.get(), ec);
				closeConnection(clients, mut, current);
				return;
			}
			hash += ustr(rectimestr);
			current->read(inbuf.get(), rsa.size(), ec);
			if(ec) {
				_logger.update(chstr(ec.message(), COLOR_PAIR(3)));
				closeConnection(clients, mut, current);
				return;
			}
			ustring messign(rsa.size(), ' ');
			inbuf.read(&messign[0], rsa.size());
			if(rsa.decrypt(messign, rsa.size(), true) != hash) {
				_logger.update(chstr("User sign isn't valid!", COLOR_PAIR(3)));
				outbuf.clear();
				outbuf.writeByte(0);
				current->write(outbuf.get(), ec);
				closeConnection(clients, mut, current);
				return;
			}
			_logger.update(chstr("User successfully logged in: ", COLOR_PAIR(4))+chstr((const char*)uname.c_str()));
		
			sessionKey = getRandUString(32);
			outbuf.clear();
			outbuf.writeByte(1);
			outbuf.write(rsa.encrypt(sessionKey, sessionKey.length(), true).c_str(), rsa.size());
			current->write(outbuf.get(), ec);
			break;
		}
	}
	closeConnection(clients, mut, current);
}


atomic <bool> locked; //TODO: Maybe something else?


void acceptByClient(const boost::system::error_code& ec) {
	if(ec) {
		messageWindow(chstr("Error"), chstr(ec.message()), COLOR_PAIR(3), COLOR_PAIR(3));
		_logger.update(chstr(ec.message(), COLOR_PAIR(3)));
		locked = false;
		return;
	}
}
void handleByClient(vector<std::shared_ptr<clientData>>& clients, shared_ptr<boost::mutex> mut, std::shared_ptr<clientData> current) {
	current->mut->lock();
	_logger.update(chstr("Trying to login to ", COLOR_PAIR(4))+chstr(current->ep->address().to_string()));
	current->mut->unlock();
	
	ustring sessionKey;
	netBuffer inbuf;
	netBuffer outbuf;
	boost::system::error_code ec;
	
	ustring dataToSign = sha256(ununame, ununame.length());
	dataToSign += ustr(to_iso_string(second_clock::local_time()));
	rsamut.lock();
	ustring messign = localrsa.encrypt(dataToSign, dataToSign.length(), false);
	rsamut.unlock();
	if(messign == ustring()) {
		messageWindow(chstr("Error"), chstr("RSA sign failed"), COLOR_PAIR(3), COLOR_PAIR(3));
		_logger.update(chstr("RSA sign failed", COLOR_PAIR(3)));
		closeConnection(clients, mut, current);
		return;
	}
	
	outbuf.clear();
	outbuf.writeByte(1);
	outbuf.write(dataToSign.c_str(), dataToSign.length());
	outbuf.write(messign.c_str(), messign.length());
	current->write(outbuf.get(), ec);
	if(ec) {
		messageWindow(chstr("Error"), chstr(ec.message()), COLOR_PAIR(3), COLOR_PAIR(3));
		_logger.update(chstr(ec.message(), COLOR_PAIR(3)));
		closeConnection(clients, mut, current);
		locked = false;
		return;
	}
	current->read(inbuf.get(), 1, ec);
	if(ec) {
		messageWindow(chstr("Error"), chstr(ec.message()), COLOR_PAIR(3), COLOR_PAIR(3));
		_logger.update(chstr(ec.message(), COLOR_PAIR(3)));
		closeConnection(clients, mut, current);
		locked = false;
		return;
	}
	
	
	switch(inbuf.readByte()) {
		case 0: {
			_logger.update(chstr("Login failed: trying to register", COLOR_PAIR(3)));
			current->reconnect(ec);
			if(ec) {
				messageWindow(chstr("Error"), chstr(ec.message()), COLOR_PAIR(3), COLOR_PAIR(3));
				_logger.update(chstr(ec.message(), COLOR_PAIR(3)));
				closeConnection(clients, mut, current);
				locked = false;
				return;
			}
			rsamut.lock();
			ustring pub = localrsa.toUstring(true); 
			rsamut.unlock();	
			outbuf.clear();
			outbuf.writeByte(0);
			outbuf.writeShort(pub.length());
			outbuf.write(pub.c_str(), pub.length());
			current->write(outbuf.get(), ec);
			if(ec) {
				messageWindow(chstr("Error"), chstr(ec.message()), COLOR_PAIR(3), COLOR_PAIR(3));
				_logger.update(chstr(ec.message(), COLOR_PAIR(3)));
				closeConnection(clients, mut, current);
				locked = false;
				return;
			}
			
			current->read(inbuf.get(), 1, ec);
			if(ec) {
				messageWindow(chstr("Error"), chstr(ec.message()), COLOR_PAIR(3), COLOR_PAIR(3));
				_logger.update(chstr(ec.message(), COLOR_PAIR(3)));
				closeConnection(clients, mut, current);
				locked = false;
				return;
			}
			ustring key;
			switch(inbuf.readByte()) {
				case 0: {
					messageWindow(chstr("Error"), chstr("Some error occured"), COLOR_PAIR(3), COLOR_PAIR(3));
					_logger.update(chstr("Some error occured", COLOR_PAIR(3)));
					closeConnection(clients, mut, current);
					locked = false;
					return;
				}
				
				case 1: {
					boost::unique_lock<boost::mutex> lock(rsamut);
					current->read(inbuf.get(), localrsa.size(), ec);
					if(ec) {
						messageWindow(chstr("Error"), chstr(ec.message()), COLOR_PAIR(3), COLOR_PAIR(3));
						_logger.update(chstr(ec.message(), COLOR_PAIR(3)));
						closeConnection(clients, mut, current);
						locked = false;
						return;
					}
					ustring data(localrsa.size(), ' ');
					inbuf.read(&data[0], localrsa.size());
					key = localrsa.decrypt(data, data.length(), false);
					if(key == ustring()) {
						messageWindow(chstr("Error"), chstr("Decrypt error: temp key"), COLOR_PAIR(3), COLOR_PAIR(3));
						_logger.update(chstr("Decrypt error: temp key", COLOR_PAIR(3)));
						closeConnection(clients, mut, current);
						locked = false;
						return;
					}
					break;
				}
				
				
				case 2: {
					string _key = dialogWindow(chstr("Enter web key:"), 32, true, COLOR_PAIR(3), COLOR_PAIR(3));
					if(_key == "") {
						messageWindow(chstr("Error"), chstr("Web key can't be empty"), COLOR_PAIR(3), COLOR_PAIR(3));
						closeConnection(clients, mut, current);
						locked = false;
						return;
					}
					key = (const unsigned char*)_key.c_str();
					break;
				}
			}
			
			ustring chdata = encryptAES(ununame, ununame.length(), key);
			outbuf.clear();
			outbuf.writeShort(chdata.length());
			outbuf.write(chdata.c_str(), chdata.length());
			current->write(outbuf.get(), ec);
			if(ec) {
				messageWindow(chstr("Error"), chstr(ec.message()), COLOR_PAIR(3), COLOR_PAIR(3));
				_logger.update(chstr(ec.message(), COLOR_PAIR(3)));
				closeConnection(clients, mut, current);
				locked = false;
				return;
			}
			current->read(inbuf.get(), 1, ec);
			if(ec) {
				messageWindow(chstr("Error"), chstr(ec.message()), COLOR_PAIR(3), COLOR_PAIR(3));
				_logger.update(chstr(ec.message(), COLOR_PAIR(3)));
				closeConnection(clients, mut, current);
				locked = false;
				return;
			}
			switch(inbuf.readByte()) {
				case 0: {
					messageWindow(chstr("Error"), chstr("Some error occured (the username is taken?)"), COLOR_PAIR(3), COLOR_PAIR(3));
					_logger.update(chstr("Some error occured (the username is taken?)", COLOR_PAIR(3)));
					closeConnection(clients, mut, current);
					locked = false;
					return;
				}
				
				case 1: {
					boost::unique_lock<boost::mutex> lock(rsamut);
					current->read(inbuf.get(), localrsa.size(), ec);
					if(ec) {
						messageWindow(chstr("Error"), chstr(ec.message()), COLOR_PAIR(3), COLOR_PAIR(3));
						_logger.update(chstr(ec.message(), COLOR_PAIR(3)));
						closeConnection(clients, mut, current);
						locked = false;
						return;
					}
					ustring data(localrsa.size(), ' ');
					inbuf.read(&data[0], localrsa.size());
					sessionKey = localrsa.decrypt(data, data.length(), false);
					if(sessionKey == ustring()) {
						messageWindow(chstr("Error"), chstr("Decrypt error: session key"), COLOR_PAIR(3), COLOR_PAIR(3));
						_logger.update(chstr("Decrypt error: session key", COLOR_PAIR(3)));
						closeConnection(clients, mut, current);
						locked = false;
						return;
					}
					break;
				}
			}
			
			messageWindow(chstr("Success"), chstr("Successfully registered"), COLOR_PAIR(4), COLOR_PAIR(4));
			_logger.update(chstr("Successfully registered", COLOR_PAIR(4)));	
			break;
		}
		
		case 1: {
			boost::unique_lock<boost::mutex> lock(rsamut);
			current->read(inbuf.get(), localrsa.size(), ec);
			if(ec) {
				messageWindow(chstr("Error"), chstr(ec.message()), COLOR_PAIR(3), COLOR_PAIR(3));
				_logger.update(chstr(ec.message(), COLOR_PAIR(3)));
				closeConnection(clients, mut, current);
				locked = false;
				return;
			}
			ustring data(localrsa.size(), ' ');
			inbuf.read(&data[0], localrsa.size());
			sessionKey = localrsa.decrypt(data, data.length(), false);
			if(sessionKey==ustring()) {
				messageWindow(chstr("Error"), chstr("Decrypt error: session key"), COLOR_PAIR(3), COLOR_PAIR(3));
				_logger.update(chstr("Decrypt error: session key", COLOR_PAIR(3)));
				closeConnection(clients, mut, current);
				locked = false;
				return;
			}
			messageWindow(chstr("Success"), chstr("Successfully logged in"), COLOR_PAIR(4), COLOR_PAIR(4));
			_logger.update(chstr("Successfully logged in", COLOR_PAIR(4)));
			break;
		}
		
		case 2: {
			messageWindow(chstr("Error"), chstr("Connection too slow"), COLOR_PAIR(3), COLOR_PAIR(3));
			_logger.update(chstr("Connection too slow", COLOR_PAIR(3)));	
			closeConnection(clients, mut, current);
			locked = false;
			return;
		}
	
	}
	locked = false;
	closeConnection(clients, mut, current);
}




string toString(uint16_t a) {
	char buf[256];
	sprintf(buf, "%hu", a);
	return string(buf);
}

void shutdown(int err) {
	endwin();
	freeOpenSSL();
	exit(err);
}
/*
class dialog {
	string _uname;
	time_t _lastRead;
	fstream _file;
	public:
	dialog(string uname) : 
			_uname(uname), 
			_lastRead(0), 
			_file() {
	}
	bool open() {
		_file.open((path("messages")/path(uname+".dlg")).string().c_str(), ios_base::binary|ios_/base::in|ios_base::out|ios_base::app|ios_base::ate);
		if(!_file.is_open())
			return false;
	}
	void update(int d) {
		if(d>0) {
			_file.seekg(-4, _file.cur);
			uint32_t off;
			_file.read((char*)&off, 4);
			_file.seekg(-(int32_t)off, _file.cur);
			
		}
		else {
			
		}
	}
	time_t getLastTime() {
		
	}
	
};*/


void initNCurses() {
	initscr();
	noecho();
	curs_set(FALSE);
	if(!has_colors()) {
		endwin();
		cout << "Terminal has no colors" << endl;
		exit(1);
    }
    start_color();
    init_pair(1, COLOR_CYAN, COLOR_BLACK);
    init_pair(2, COLOR_YELLOW, COLOR_BLACK);
    init_pair(3, COLOR_RED, COLOR_BLACK);
    init_pair(4, COLOR_GREEN, COLOR_BLACK);
    attron(COLOR_PAIR(1));
	clear();
}



void createDirectories() {
	if(!is_directory("users"))
		create_directory(path("users"));
	if(!is_directory("userdata"))
		create_directory(path("userdata"));
	if(!is_directory("messages"))
		create_directory(path("messages"));
}


void joinSystem() {
	path fname;
	for(auto a = directory_iterator(path(".")); a!=directory_iterator(); ++a)
		if(extension(a->path())==".pem") {
			fname = a->path();
			break;
		}
	if(fname==path()) {
		string uname = dialogWindow(chstr("Enter username"), 5, false, COLOR_PAIR(1), COLOR_PAIR(1));
		if(uname=="") {
			messageWindow(chstr("Error"), chstr("Username can't be empty!"), COLOR_PAIR(1), COLOR_PAIR(1));
			shutdown(1);
		}
		clear();
		ununame = ustr(uname);
		string upass = dialogWindow(chstr("Enter password"), 5, true, COLOR_PAIR(1), COLOR_PAIR(1));
		if(upass=="") {
			messageWindow(chstr("Error"), chstr("Password can't be empty!"), COLOR_PAIR(1), COLOR_PAIR(1));
			shutdown(1);
		}
		clear();
		ustring hashupass = sha256(ustr(upass), upass.length());
		rsamut.lock();
		localrsa = pairRSA(2048, 17);
		ustring rsaPrivate = localrsa.toUstring(false);
		rsamut.unlock();
		ustring chrsaPrivate = encryptAES(rsaPrivate, rsaPrivate.length(), hashupass);
		if(chrsaPrivate==ustring()) {
			messageWindow(chstr("Error"), chstr("Can't encrypt RSA key"), COLOR_PAIR(1), COLOR_PAIR(1));
			shutdown(1);
		}
		std::ofstream file((uname+".pem").c_str(), ios_base::binary|ios_base::out);
		if(!file.is_open()) {
			messageWindow(chstr("Error"), chstr("Can't open file ")+chstr(uname)+chstr(".pem for writing!"), COLOR_PAIR(1), COLOR_PAIR(1));
			shutdown(1);
		}
		file.write((const char*)chrsaPrivate.c_str(), chrsaPrivate.length());
		file.close();
	}
	else {
		string upass = dialogWindow(chstr("Enter password"), 5, true, COLOR_PAIR(1), COLOR_PAIR(1));
		if(upass=="") {
			messageWindow(chstr("Error"), chstr("Password can't be empty!"), COLOR_PAIR(1), COLOR_PAIR(1));
			shutdown(1);
		}
		clear();
		ustring hashupass = sha256(ustr(upass), upass.length());
		ununame = ustr(fname.stem().string());
		std::ifstream file(fname.string().c_str(), ios_base::binary|ios_base::in);
		if(!file.is_open()) {
			messageWindow(chstr("Error"), chstr("Can't open file ")+chstr(fname.filename().string())+chstr(" for reading!"), COLOR_PAIR(1), COLOR_PAIR(1));
			shutdown(1);
		}
		file.seekg(0, ios_base::end);
		size_t fsize = file.tellg();
		file.seekg(0, ios_base::beg);
		ustring chpriv(fsize, ' ');
		file.read((char*)&chpriv[0], fsize);
		file.close();
		ustring priv = decryptAES(chpriv, chpriv.length(), hashupass);
		if(priv==ustring()) {
			messageWindow(chstr("Error"), chstr("Can't decrypt RSA key"), COLOR_PAIR(1), COLOR_PAIR(1));
			shutdown(1);
		}
		rsamut.lock();
		localrsa = pairRSA(priv, false);
		if(localrsa.get()==NULL) {
			messageWindow(chstr("Error"), chstr("RSA corrupted"), COLOR_PAIR(1), COLOR_PAIR(1));
			shutdown(1);
		}	
		rsamut.unlock();
	}
} 


int main() {
#if defined(__WIN32__)
	SetConsoleOutputCP(866);
#endif
	
	initNCurses();
	initOpenSSL();
	createDirectories();
	joinSystem();
	
	
	/*
	vector<string> inbox;
	vector<string> outbox;
	for(auto a = directory_iterator(path("messages")/path("inbox")); a!=directory_iterator(); ++a)
		if(extension(a->path())==".dlg") 
			inbox.push_back(stem(a->path()));
	for(auto a = directory_iterator(path("messages")/path("outbox")); a!=directory_iterator(); ++a)
		if(extension(a->path())==".dlg") 
			outbox.push_back(stem(a->path()));
	*/
	
	
	window menuW(getmaxy(stdscr), getmaxx(stdscr)/4, 0, 0, COLOR_PAIR(1), COLOR_PAIR(1));
	window mainW(getmaxy(stdscr), getmaxx(stdscr)/2, 0, getmaxx(menuW.decor), COLOR_PAIR(1), COLOR_PAIR(1));
	window usersW(getmaxy(stdscr), getmaxx(stdscr)/4, 0, getmaxx(menuW.decor)+getmaxx(mainW.decor), COLOR_PAIR(1), COLOR_PAIR(1));
	keypad(menuW.win, TRUE);
	_logger.setWin(mainW.win);
    
	
	menuList menuL;
	menuList mainL;
	menuL.add("Forum", true, 1);
	menuL.add("Messages", true, 1);
	menuL.add("Settings", true, 1);
	menuL.add("Logger", true, 1);
	menuL.draw(menuW.win, 0, 0, getmaxx(menuW.win));
	
	uint16_t _num_port = 35665;
	string _port = toString(_num_port);    				//TODO: We're really need this?
	uint16_t _proxy_num_port = 8888;
	string _proxy_port = toString(_proxy_num_port);		//TODO: and this
	uint16_t _web_num_port = 2323;
	string _web_port = toString(_web_num_port);		//TODO: and this
	
	asyncServer _server(acceptByServer, handleByServer);
	asyncClient _clients(acceptByClient, handleByClient);
	asyncServer _proxy(acceptByServer, handleByProxy);
	asyncServer _web(acceptByServer, handleByWebServer);
	
	
	bool f = true;
	int level = 0;
	//size_t curw = getmaxx(stdscr);
	//size_t curh = getmaxy(stdscr);
	//TODO: defines of magic numbers
	while(f) {
		chtype ch = wgetch(menuW.win);
		switch(ch) {
			case KEY_UP: {
				if(level==0)
					--menuL;
				else if(level==1)
					if(_logger.status())
						--_logger;
					else
						--mainL;
				break;
			}
		
			case KEY_DOWN: {
				if(level==0)
					++menuL;
				else if(level==1)
					if(_logger.status())
						++_logger;
					else
						++mainL;
				break;
			}
			
			//ESC button
			//TODO: catch ESC on Linux faster
			case 27: {
				if(level==0)
					f = false;
				else if(level==1) {
					_logger.stop();
					level = 0;
					mainL.clear();
					mainW.redraw();
				}
				break;
			}
			
			case '\n': {
				if(level==0) {
					level=1;
					switch((int)menuL) {
						case 0: {
							mainL.add("There is nothing", true, 0);
							mainL.add("now", true, 0);
							break;
						}
						
						case 1: {
							mainL.add("---write message---", false, 1);
							break;
						}
						
						case 2: {
							mainL.add("Server:", false, 1);
							mainL.add("Port: "+_port, true, 1);
							mainL.add("Start server", true, 1, COLOR_PAIR(2)|A_BOLD);
							mainL.add("", false, 1);
							mainL.add("Client:", false, 1);
							mainL.add("Connect", true, 1, COLOR_PAIR(2)|A_BOLD);
							mainL.add("", false, 1);
							mainL.add("Proxy server:", false, 1);
							mainL.add("Port: "+_proxy_port, true, 1);
							mainL.add("Start proxy", true, 1, COLOR_PAIR(2)|A_BOLD);
							mainL.add("", false, 1);
							mainL.add("Web server:", false, 1);
							mainL.add("Port: "+_web_port, true, 1);
							mainL.add("Start web server", true, 1, COLOR_PAIR(2)|A_BOLD);
							break;
						}
						
						case 3: {
							mainL.clear();
							_logger.start();
							break;
						}
					}
				}
				else switch((int)menuL) {
					case 0: {
					break;
					}
					
					case 1: {
					break;
					}
					
					case 2: {
						switch((int)mainL) {
							case 1: {
								string port = dialogWindow(chstr("Select port"), 5, false, COLOR_PAIR(1), COLOR_PAIR(1));
								uint16_t np;
								if(sscanf(port.c_str(), "%hu", &np)==1) {
									_port = toString(np);
									_num_port = np;
									mainL[1] = chstr("Port: ")+chstr(_port);
								}
								mainW.redraw();
								break;
							}
							
							case 2: {
								menuL = 2;
								menuL.draw(menuW.win, 0, 0, getmaxx(menuW.win));
								_server.start(_num_port); //TODO: try catch exceptions
								_logger.update(chstr("Server started", COLOR_PAIR(4)));
								mainW.redraw();
								mainL.clear();
								_logger.start();
								break;
							}
							
							case 5: {
								string address = dialogWindow(chstr("Enter address"), 5, false, COLOR_PAIR(1), COLOR_PAIR(1));
								_clients.connect(address, _num_port); //TODO: try catch exceptions
								if(!_clients.status())
									_clients.start();
								locked = true;
								while(locked);
								break;
							}
							
							
							case 8: {
								string port = dialogWindow(chstr("Select port"), 5, false, COLOR_PAIR(1), COLOR_PAIR(1));
								uint16_t np;
								if(sscanf(port.c_str(), "%hu", &np)==1) {
									_proxy_port = toString(np);
									_proxy_num_port = np;
									mainL[8] = chstr("Port: ")+chstr(_port);
								}
								mainW.redraw();
								break;
							}
							
							case 9: {
								menuL = 2;
								menuL.draw(menuW.win, 0, 0, getmaxx(menuW.win));
								_proxy.start(_proxy_num_port); //TODO: try catch exceptions
								_logger.update(chstr("Proxy started", COLOR_PAIR(4)));
								mainW.redraw();
								mainL.clear();
								_logger.start();
								break;
							}
							
							case 12: {
								string port = dialogWindow(chstr("Select port"), 5, false, COLOR_PAIR(1), COLOR_PAIR(1));
								uint16_t np;
								if(sscanf(port.c_str(), "%hu", &np)==1) {
									_web_port = toString(np);
									_web_num_port = np;
									mainL[12] = chstr("Port: ")+chstr(_port);
								}
								mainW.redraw();
								break;
							}
							
							case 13: {
								menuL = 2;
								menuL.draw(menuW.win, 0, 0, getmaxx(menuW.win));
								_web.start(_web_num_port); //TODO: try catch exceptions
								_logger.update(chstr("Web server started", COLOR_PAIR(4)));
								mainW.redraw();
								mainL.clear();
								_logger.start();
								break;
							}
						}
						break;
					}
					
					case 3: {
					break;
					}
					
				}
				break;
			}
		}
		//TODO: redraw window
		//Not work...
		/*if((curw!=getmaxx(stdscr))||(curh!=getmaxy(stdscr))) {
			shutdown(0);
			curw = getmaxx(stdscr);
			curh = getmaxy(stdscr);
			menuW.update(getmaxy(stdscr), getmaxx(stdscr)/4, 0, 0);
			mainW.update(getmaxy(stdscr), getmaxx(stdscr)/2, 0, getmaxx(menuW.decor));
			usersW.update(getmaxy(stdscr), getmaxx(stdscr)/4, 0, getmaxx(menuW.decor)+getmaxx(mainW.decor));
		}*/
		//clear();
		//refresh();
		//menuW.redraw();
		//mainW.redraw();
		//usersW.redraw();
		menuL.draw(menuW.win, 0, 0, getmaxx(menuW.win));
		mainL.draw(mainW.win, 0, 0, getmaxx(mainW.win));
	}
	_server.stop();
	_clients.stop();
	_proxy.stop();
	_web.stop();
	keypad(menuW.win, FALSE);
	shutdown(0);
	return 0;
}
